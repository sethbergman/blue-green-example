# Blue-Green Example

This project demonstrates an example
[blue-green deployment](https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html#blue-green-deployment)
implementation for [Kubernetes on GitLab](https://docs.gitlab.com/ee/user/project/clusters/)
using [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

## Demo

For more details on how to operate the configuration in this example, this
[video demonstration](https://www.youtube.com/watch?v=ymgwt2NUbd4) shows it in action.

## Caveats

When using this configuration, the `Open live environment` button does not show
up for some environments in your **Operations > Environments** page. This does
not necessarily mean that the environment is not live. You should be able to
access live environments by going to their URL directly.

You may also see the error `Kubernetes deployment not found` for the
`production` environment. This is because there are no Kubernetes resources
dedicated to that environment. Instead, it is the blue or green environments
themselves that will act as the production environment when they are switched to
being the active environment.

There is further documentation on drawbacks, considerations, and issues you may
encounter written inline in the
[`gitlab-ci.yml` file in this repository](.gitlab-ci.yml).
